FROM alpine:3.13.1
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache alpine-sdk ccache cmake
RUN adduser -s /bin/sh -D builder
RUN adduser builder abuild

# Generate keys
#
# TODO: It doesn't matter if we publish them because they're private
# repositories.
# USER builder
# RUN abuild-keygen
# RUN echo /home/builder/.abuild/*.rsa \
#   | sed "s/^/PACKAGER_PRIVKEY=/" \
#   >> /home/builder/.abuild/abuild.conf

ENV PATH=/usr/lib/ccache/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
